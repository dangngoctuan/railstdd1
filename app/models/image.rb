class Image < ActiveRecord::Base
  default_scope { order(menu_image: :desc) }
  belongs_to :article, optional: true
  validates :image_head_line, presence: true
  validates :menu_image, presence: true
  validates :url, presence: true
  validates :image_likes, presence: true
  def show_image_likes
    self.image_likes
  end
end
