class Article < ActiveRecord::Base
  has_many :texts, dependent: :destroy
  has_many :images, dependent: :destroy
 validates :title, presence: true
 validates :article_likes, presence: true
  def by_search
    self.texts + self.images
  end

  def by_published
    self.published
  end

  def by_finished
    self.to_s
  end

  def show_likes
    self.article_likes
  end

  def menu_text
    self.texts
  end

  def menu_image
    self.images
  end

  def show_times
    self.created_at < Time.now
  end

  def article_published_day
    self.published_day < DateTime.now
  end
end
