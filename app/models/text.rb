class Text < ActiveRecord::Base
  default_scope { order(menu_text: :desc) }
  belongs_to :article, optional: true
  validates :text_head_line, presence: true
  validates :menu_text, presence: true
  validates :message, presence: true
  validates :text_likes, presence: true
  def show_text_likes
    self.text_likes
  end
end
