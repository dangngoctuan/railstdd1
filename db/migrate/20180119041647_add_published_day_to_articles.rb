class AddPublishedDayToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :published_day, :datetime
  end
end
