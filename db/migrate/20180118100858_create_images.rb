class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :image_head_line
      t.integer :menu_image
      t.string :url
      t.integer :image_likes
    end
  end
end
