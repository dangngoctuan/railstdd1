class CreateTexts < ActiveRecord::Migration[5.1]
  def change
    create_table :texts do |t|
      t.string :text_head_line
      t.integer :menu_text
      t.string :message
      t.integer :text_likes
    end
  end
end
