# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Article.create!(title: 'Java Book', article_likes: 30)
Article.create!(title: 'HTML Book', article_likes: 25)
Text.create!(text_head_line: 'abc', menu_text: 1, message: 'dangtuan', text_likes: 20)
Text.create!(text_head_line: 'nndjskfj', menu_text: 2, message: 'jfdsjkj', text_likes: 20)
Image.create!(image_head_line: 'bcd' , menu_image: 1, url: 'http://abc/bc.jpg', image_likes: 25)
Image.create!(image_head_line: 'fjsk' , menu_image: 2, url: 'http://yhd/hfjdh.jpg', image_likes: 15)
