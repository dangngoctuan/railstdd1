FactoryBot.define do
  factory :article do
    title 'Ruby Book'
    article_likes 10
    published true
    published_day DateTime.new(2018,1,18,5,3,2)
  end

  factory :image do
    image_head_line 'jfjjfjf'
    menu_image 1
    url 'hfjjsjfs'
    image_likes 15
    article_id 1
  end

  factory :text do
    text_head_line 'hfdjf'
    menu_text 1
    message 'jfjfkdjkj'
    text_likes 10
    article_id 1
  end
end
