require 'rails_helper'
RSpec.describe Text, type: :model do
    context 'association' do
      it { should belong_to(:article) }
    end
    context 'validates' do
      it { should validate_presence_of(:text_head_line) }
      it { should validate_presence_of(:menu_text) }
      it { should validate_presence_of(:message) }
      it { should validate_presence_of(:text_likes) }
    end
    before do
      @text = create(:text)
    end
    it 'show text_likes' do
      expect(@text.show_text_likes).to eq 10
    end

end
