require 'rails_helper'
RSpec.describe Article, type: :model do
    context 'validation' do
      it { should have_many(:texts) }
      it { should have_many(:images) }
      it { should validate_presence_of(:title) }
      it { should validate_presence_of(:article_likes) }
    end
    context 'write test for method' do
      before do
        @art = create(:article)
        @text = create(:text)
        @text1 = create(:text, menu_text: 2)
        @text2= create(:text, menu_text: 3)
        @img = create(:image)
        @img1 = create(:image, menu_image: 3)
        @img2 = create(:image, menu_image: 4)
      end

      it 'instance of Article have method respon text and img' do
        expect(@art.by_search).to eq [@text2,@text1,@text,@img2,@img1,@img]
      end

      it 'article published?' do
        expect(@art.by_published).to eq true
      end

      it ' published fisnish ok ' do
        expect(@art.by_finished).to eq @art.to_s
      end

      it 'show likes' do
        expect(@art.show_likes).to eq 10
      end

      it 'show menu_text' do
        expect(@art.menu_text).to eq [@text2,@text1,@text]
      end

      it 'show menu_image' do
        expect(@art.menu_image).to eq [@img2,@img1,@img]
      end

      it ' show times now when published' do
        expect(@art.show_times).to eq true
      end

      it 'published article < time.now' do
      expect(@art.article_published_day).to eq true
      end

    end

end
