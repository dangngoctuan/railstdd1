require 'rails_helper'
RSpec.describe Image, type: :model do
    context 'association' do
      it { should belong_to(:article) }
    end

    context 'validation' do
      it { should validate_presence_of(:image_head_line) }
      it { should validate_presence_of(:menu_image) }
      it { should validate_presence_of(:url) }
      it { should validate_presence_of(:image_likes) }
    end
    before do
      @img = create(:image)
    end
    it ' show image_likes' do
      expect(@img.show_image_likes).to eq 15
    end

end
